package com.mindgame.truelove.quotesin.english

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.support.v4.content.FileProvider
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class AppUtils {

     fun shareImage(bmp: Bitmap,ctx:Context) {

        val share = Intent(Intent.ACTION_SEND)
         share.putExtra(Intent.EXTRA_TEXT, "Please Download the app")
         share.putExtra(Intent.EXTRA_TEXT, "Hey please check this application \n https://play.google.com/store/apps/details?id="+AdObject.PACKAGE_NAME);
        share.type = "image/*"

        val bytes = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

        val f = File(ItemDataset.APP_DIR,"temp.jpg")
         val uriShare = FileProvider.getUriForFile(ctx,
                 AdObject.PACKAGE_NAME + ".fileprovider", f)

        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        share.putExtra(Intent.EXTRA_STREAM, uriShare)
        try {
            ctx.startActivity(Intent.createChooser(share, "Share via"))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(ctx, "Please Connect To Internet", Toast.LENGTH_LONG)
                    .show()
        }

    }

    fun loadGalleryFromAssets(context: Context){
        ItemDataset.topics_icons= getGalleryIconListwithPath(context.assets.list("gallery_icons"))
        val galleryDirectoryList=context.assets.list("app_images")
        ItemDataset.items = ArrayList<Item>()
        galleryDirectoryList!!.forEachIndexed { index, it ->
            ItemDataset.items.add(
                    Item(topic_title = it,
                        topic_icon = ItemDataset.topics_icons[index],
                        menus = getImageListFromGalleryDirectory(context, it)
                    ))
        }

        }

    private fun getGalleryIconListwithPath(galleryIconList: Array<String>?): ArrayList<String> {
        val iconsList:ArrayList<String> = ArrayList()
        galleryIconList!!.forEach { iconsList.add("gallery_icons/$it") }
        return iconsList
    }

    private fun getImageListFromGalleryDirectory(context: Context, galleryDirectory: String?):ArrayList<String> {
        val imagesList:ArrayList<String> = ArrayList()
        context.assets.list("app_images/$galleryDirectory")!!.toCollection(ArrayList<String>()).forEach {
                imagesList.add("app_images/$galleryDirectory/$it")
        }
        return imagesList
    }
}



