package com.mindgame.truelove.quotesin.english

import android.app.Activity
import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.fragment_book_mark_item.view.*
import kotlinx.android.synthetic.main.pager_item.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BookMarkItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class BookMarkItemFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var itemTitle: MenuItem? = null
    lateinit var toolbar: android.support.v7.widget.Toolbar
    lateinit var imgBookMarkImage: ImageView
    lateinit var vpBookMarkImage: ViewPager
    var bookmark_items = ItemDataset.mDbHelper.getAllBookMarks()
    var mImages: ArrayList<String> = ItemDataset.mDbHelper.getAllBookMarks()
    private val vpAdapter = BookMarkImagePagerAdapter(mImages)
    private var mAdCount = 0
    private lateinit var appInterfaces:AppInterfaces
    private lateinit var mLayoutInflater: LayoutInflater

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mLayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


        // Inflate the rate_me_layout for this fragment
        val v = inflater.inflate(R.layout.fragment_book_mark_item, container, false)
        vpBookMarkImage = v.vpBookMarkImage


/*----------------ACTIONS FOR TOOLBAR-------------------------*/
        //setup path action bar
        val act: AppCompatActivity = activity as AppCompatActivity
        act.setSupportActionBar(v.toolbar_bookmarks_item)

        setHasOptionsMenu(true)
        toolbar = v.toolbar_bookmarks_item

/*----------------ACTIONS FOR TOOLBAR-------------------------*/


/*--------------------LOAD THE VIEW PAGER--------------------*/
        vpBookMarkImage.adapter = vpAdapter
        vpBookMarkImage.currentItem = ItemDataset.position_bookmark

        toolbar.title = "${vpBookMarkImage.currentItem + 1}/${bookmark_items.size}"

        v.vpBookMarkImage.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {

            // optional
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            // optional
            override fun onPageSelected(position: Int) {
                mAdCount++
                if (mAdCount % 5 == 0) {
                    AdObject.admob.loadNextScreen {  }

                }
                ItemDataset.position_bookmark = position //Save the position of the current item

            }

            // optional
            override fun onPageScrollStateChanged(state: Int) {

                    toolbar.title = "${vpBookMarkImage.currentItem + 1}/${bookmark_items.size}"



            }
        })
/*--------------------LOAD THE VIEW PAGER------------------*/
     return v

    }


    override fun onCreateOptionsMenu(menu: android.view.Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_bookmarks, menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        var img_selected = bookmark_items.elementAt(vpBookMarkImage.currentItem)
        val img_bmp = BitmapFactory.decodeStream(activity!!.assets.open(img_selected) )
        when (item!!.itemId) {

         /*-----------WALLPAPER--------------*/
            R.id.btnWallpaper -> {

                    val myWallpaperManager = WallpaperManager.getInstance(context)

                    try {
                        myWallpaperManager.clear()
                        myWallpaperManager.setBitmap(img_bmp)
                        Toast.makeText(context,
                                "Wallpaper Set Successfully!!", Toast.LENGTH_SHORT)
                                .show()

                    } catch (e: IOException) {
                        // TODO Auto-generated catch block
                        Toast.makeText(context,
                                "Setting WallPaper Failed!!", Toast.LENGTH_SHORT)
                                .show()

                }
            }
        /*---SHARE THE ITEM-----------*/
            R.id.btnShare -> {
                AppUtils().shareImage(img_bmp,context as Context)
            }

      /*---DELETE THE ITEM-----------*/
            R.id.btnDelete -> {

                var status = ItemDataset.mDbHelper.deleteBookMark(img_selected)
                if (status) {
                    mImages.remove(img_selected)
                }
                //Toast.makeText(context,"Deleted ${img_selected} Successfully.",Toast.LENGTH_SHORT).show()
                Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Deleted ${img_selected} Successfully.", Snackbar.LENGTH_SHORT).show()
//                mImages =  ItemDataset.mDbHelper.getAllBookMarks()
                bookmark_items = ItemDataset.mDbHelper.getAllBookMarks()

                if (mImages.size > 0) {
//                    vpBookMarkImage.adapter!!.notifyDataSetChanged()
//                    img_selected = bookmark_items.elementAt(vpBookMarkImage.currentItem+1)
//                    vpBookMarkImage.currentItem+=1
                    vpBookMarkImage.adapter!!.notifyDataSetChanged()
                    toolbar.title = "${vpBookMarkImage.currentItem + 1}/${bookmark_items.size}"
                } else {
                    AdObject.fragmentsStack.pop() //remove the entry for - BookMarkItemFragment
                    AdObject.fragmentsStack.pop() //remove the entry for - BookMarkFragment
                    appInterfaces.loadItem() //Load the Image Menus
                }


            }


        /*--LOAD THE BOOK MARKS SCREEN-----*/
            R.id.btnBookMarksList->{
                AdObject.admob.loadNextScreen {
                   appInterfaces.loadBookMarkMenu()
                }
            }

            R.id.btnRate->{
            AdObject.rateApp(activity as Context)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun loadItem(index: Int) {

        try {
            ItemDataset.MENU_ID += index //Increment the Menu Id to get the correct Item
            ItemDataset.position += index // Increment the position to get the correct title

//            toolbar.title = "${ItemDataset.position + 1}." + ItemDataset.currentMenuTitle
            AdObject.admob.loadNextScreen {


            }
        } catch (ex: ArrayIndexOutOfBoundsException) {
            //Toast.makeText(activity, "Last Item or First item reached!", Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Last Item or First item reached!", Snackbar.LENGTH_SHORT).show()
        }
    }




    private inner class BookMarkImagePagerAdapter(mImages:Collection<String>) : PagerAdapter() {


        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun getCount(): Int {
            return mImages.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as ImageView
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false)
            val pref = context!!.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
            val iconPath = ItemDataset.ASSET_URI + mImages.elementAt(position)

            GlideApp.with(activity as FragmentActivity)
                    .load(Uri.parse(iconPath))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(itemView.suit_image)
            itemView.suit_image.setOnTouchListener(MultiTouchListener())
            (container as ViewPager).addView(itemView)


            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as ImageView)
        }
    }



    fun saveImage(b: Bitmap, img_name:String) {


        val fullPath =  ItemDataset.APP_DIR
        try {
            val dir = fullPath
            if (!dir.exists()) {
                dir.mkdirs()
            }
            var fOut: OutputStream? = null
            val file = File(fullPath, "$img_name")
            if (file.exists())
                file.delete()
            file.createNewFile()
            fOut = FileOutputStream(file)
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.WEBP, 100, fOut)
            fOut.flush()
            fOut.close()
            //Toast.makeText(activity,"Image $img_name saved successfully.", Toast.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.message)
            //Toast.makeText(activity,"Error in saving Image $img_name.", Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Error in saving Image $img_name.", Snackbar.LENGTH_SHORT).show()
        }

    }

}
