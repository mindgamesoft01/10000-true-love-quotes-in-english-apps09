package com.mindgame.truelove.quotesin.english

import android.app.IntentService
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import java.io.IOException
import java.util.*


// TODO: Rename actions, choose action names that describe tasks that this
// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
private const val ACTION_FOO = "com.mindgame.kotlintemplatefor.images.action.FOO"
private const val ACTION_BAZ = "com.mindgame.kotlintemplatefor.images.action.BAZ"

// TODO: Rename parameters
private const val EXTRA_PARAM1 = "com.mindgame.kotlintemplatefor.images.extra.PARAM1"
private const val EXTRA_PARAM2 = "com.mindgame.kotlintemplatefor.images.extra.PARAM2"

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
class IntentServiceIsOnline : IntentService("IntentServiceIsOnline") {
    override fun onCreate() {
        super.onCreate()
        startForeground(1, Notification())
    }

    override fun onHandleIntent(intent: Intent?) {

        val timerObj = Timer()
        val timerTaskObj = object : TimerTask() {
           override fun run() {
                //perform your action here
               AdObject.IsOnline.postValue(isOnline())
            }
        }
        timerObj.schedule(timerTaskObj, 0, 10000)



        when (intent?.action) {
            ACTION_FOO -> {
                val param1 = intent.getStringExtra(EXTRA_PARAM1)
                val param2 = intent.getStringExtra(EXTRA_PARAM2)
                handleActionFoo(param1, param2)
            }
            ACTION_BAZ -> {
                val param1 = intent.getStringExtra(EXTRA_PARAM1)
                val param2 = intent.getStringExtra(EXTRA_PARAM2)
                handleActionBaz(param1, param2)
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionFoo(param1: String, param2: String) {
        TODO("Handle action Foo")
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionBaz(param1: String, param2: String) {
        TODO("Handle action Baz")
    }


    fun isOnline():Boolean {

      if (AdObject.connectivityManager.activeNetworkInfo == null || !AdObject.connectivityManager.activeNetworkInfo.isConnectedOrConnecting){
          return false
      }


        try {
            /*Pinging to Google server*/
        val command = "ping -c 1 google.com"
        val result = Runtime.getRuntime().exec(command).waitFor() == 0
            return result

        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e:InterruptedException) {
            e.printStackTrace()
        } finally {

        }
        return false
    }
    fun isConnected(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnectedOrConnecting
    }

}
