package com.mindgame.truelove.quotesin.english

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
