package com.mindgame.truelove.quotesin.english

import java.io.File


object ItemDataset {
    var position:Int = 0
    var position_bookmark:Int = 0
    var topics = Images_Topics.topic_titles
    lateinit var topics_icons:ArrayList<String>
    lateinit var APP_DIR:File


    lateinit var items:ArrayList<Item>

    var TOPIC_ID:Int = 1
    var MENU_ID:Int = 1
    lateinit var mDbHelper: DataBaseHelper

    var ITEM_TEXT =  ""
    lateinit var item_current:Item

    lateinit var admob:AdmobUtility
    const val APP_JSON_FILE_NAME = "app_data_full.json"
//    const val ASSET_URI = "file:///android_asset/app_images/"
    const val ASSET_URI = "file:///android_asset/"

}