package com.mindgame.truelove.quotesin.english

import android.app.Activity
import android.app.WallpaperManager
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.fragment_item.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ItemFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ItemFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var itemTitle: MenuItem? = null
    private var wvWebView: WebView? = null
    private var mAdCount:Int = 0
    lateinit var toolbar: android.support.v7.widget.Toolbar
    lateinit var vpItemImage: ViewPager
    private lateinit var appInterfaces:AppInterfaces
    private lateinit var mLayoutInflater: LayoutInflater

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mLayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate the rate_me_layout for this fragment
        val v = inflater.inflate(R.layout.fragment_item, container, false)


/*----------------ACTIONS FOR TOOLBAR-------------------------*/
        //setup pthe action bar
        val act: AppCompatActivity = activity as AppCompatActivity
//        v.my_toolbar.title = "1/"+ItemDataset.item_current.menus.size
        act.setSupportActionBar(v.my_toolbar)

        setHasOptionsMenu(true)
        toolbar = v.my_toolbar
        vpItemImage = v.vpItemImage

/*----------------ACTIONS FOR TOOLBAR-------------------------*/

        val position = ItemDataset.position

        Log.e("IMG POSITION:",ItemDataset.item_current.menus.size.toString())

/*--------------------LOAD THE VIEW PAGER------------------*/
        vpItemImage.adapter = ImagePagerAdapter()
        vpItemImage.currentItem = ItemDataset.position
        toolbar.title = "${v.vpItemImage.currentItem + 1}/${ItemDataset.item_current.menus.size}"
        vpItemImage.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {



            // optional
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}



            // optional
            override fun onPageSelected(position: Int) {

                mAdCount++
                if (mAdCount % 5 == 0) {
                    AdObject.admob.loadNextScreen {  }

                } else {

                }
            }

            // optional
            override fun onPageScrollStateChanged(state: Int) {
                ItemDataset.position = vpItemImage.currentItem

                toolbar.title = "${vpItemImage.currentItem + 1}/${ItemDataset.item_current.menus.size}"


            }
        })
/*--------------------LOAD THE VIEW PAGER------------------*/
        // loadtexttoBody() //Load the image


        return v

    }

    override fun onResume() {
        super.onResume()
        toolbar.title = "${vpItemImage.currentItem + 1}/${ItemDataset.item_current.menus.size}"
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu?, inflater: MenuInflater?) {
        val mnu = inflater!!.inflate(R.menu.menu, menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        val img_bmp = BitmapFactory.decodeStream(activity!!.assets.open(ItemDataset.item_current.menus[vpItemImage.currentItem]))
        when (item!!.itemId) {

            /*-------------------WALLPAPER----------------------*/
          R.id.btnWallpaper -> {
              val myWallpaperManager = WallpaperManager.getInstance(context)

              try {
                  myWallpaperManager.clear()
                  myWallpaperManager.setBitmap(img_bmp)
                  Toast.makeText(context,
                          "Wallpaper Set Successfully!!", Toast.LENGTH_SHORT)
                          .show()

              } catch (e: IOException) {
                  // TODO Auto-generated catch block
                  Toast.makeText(context,
                          "Setting WallPaper Failed!!", Toast.LENGTH_SHORT)
                          .show()

              }
          }

        /*---SHARE THE ITEM----------------------------------*/
            R.id.btnShare -> {
                AppUtils().shareImage(img_bmp,ctx = context as Context)
            }

        /*---BOOK MARK THE ITEM-----------*/
            R.id.btnBookmark -> {
                ItemDataset.mDbHelper.addBookMark( imgName = ItemDataset.item_current.menus[vpItemImage.currentItem] )
//                Toast.makeText(context, "Saved ${ItemDataset.item_current.menus[vpItemImage.currentItem]} Successfully.", Toast.LENGTH_LONG).show()
                Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Image ${ItemDataset.item_current.menus[vpItemImage.currentItem]} saved successfully.", Snackbar.LENGTH_SHORT).show()


            }

        /*--LOAD THE BOOK MARKS SCREEN-----*/
            R.id.btnBookMarksList->{
                AdObject.admob.loadNextScreen {
                        appInterfaces.loadBookMarkMenu()
                }
            }
        /*------------RATE ME --------------*/
            R.id.btnRate->{
                AdObject.rateApp(activity as Context)
            }
        }
        return super.onOptionsItemSelected(item)
    }





    private inner class ImagePagerAdapter : PagerAdapter() {
        private val mImages = ItemDataset.item_current.menus

        override fun getCount(): Int {
            return mImages.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
//            return view === `object` as RelativeLayout
            return view === `object` as ImageView
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false)
            val pref = context!!.getSharedPreferences("MyPref", MODE_PRIVATE)


            val imageView = itemView.findViewById(R.id.suit_image) as ImageView
            val iconPath = ItemDataset.ASSET_URI + mImages[position]
            GlideApp.with(activity as FragmentActivity)
                    .load(Uri.parse(iconPath))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView)
            imageView.setOnTouchListener(MultiTouchListener())
            (container as ViewPager).addView(itemView)

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//            container.removeView(`object` as RelativeLayout)
            container.removeView(`object` as ImageView)
        }
    }


    fun saveImage(b: Bitmap, img_name:String) {


        val fullPath =  ItemDataset.APP_DIR
        try {
            val dir = fullPath
            if (!dir.exists()) {
                dir.mkdirs()
            }
            var fOut: OutputStream? = null
            val file = File(fullPath, "$img_name")
            if (file.exists())
                file.delete()
            file.createNewFile()
            fOut = FileOutputStream(file)
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.WEBP, 100, fOut)
            fOut.flush()
            fOut.close()
//            Toast.makeText(activity,"Image $img_name saved successfully.",Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Image $img_name saved successfully.", Snackbar.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.message)
//            Toast.makeText(activity,"Error in saving Image $img_name.",Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Error in saving Image $img_name.", Snackbar.LENGTH_SHORT).show()
        }

    }

}