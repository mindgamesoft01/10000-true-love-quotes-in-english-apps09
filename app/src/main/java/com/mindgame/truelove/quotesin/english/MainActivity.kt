package com.mindgame.truelove.quotesin.english

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mindgame.kotlintemplatefor.images.Worker.Companion.SHOW_RESULT
import com.mindgame.kotlintemplatefor.images.WorkerResultReceiver.Receiver
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import java.io.File
import java.lang.Math.ceil


class MainActivity : AppCompatActivity(), AppInterfaces, Receiver {

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        when (resultCode) {
            SHOW_RESULT -> if (resultData != null) {
                Snackbar.make(clMainActivity, resultData.getString("data")!!.toString(), Snackbar.LENGTH_SHORT).show()
            }
        }
    }


    private val START_SCREEN = "START_SCREEN"
    private val TOPICS = "TOPICS"
    private val MENUS = "MENUS"
    private val ITEM = "ITEM"
    private val BOOKMARK_MENU = "BOOKMARK_MENU"
    private val BOOKMARK_ITEM = "BOOKMARK_ITEM"
    private var MSG_DISPLAYED = false

    private var BANNER_LOADED = false
    private var DB_NAME = "db_temp01.db" //CHANGE THE DB NAME FOR EVERY APP

    var images_all: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /*---------------TESTING----------------------*/
        var mWorkerResultReceiver = WorkerResultReceiver(Handler())
        mWorkerResultReceiver.setReceiver(this)
        Worker.enqueueWork(this, mWorkerResultReceiver)
        /*---------------TESTING----------------------*/
        AppUtils().loadGalleryFromAssets(context = this)

//        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        images_all.addAll(assets.list("app_images").filterNotNull())



        DB_NAME = packageName.toString().replace(".", "_")
        val myDBHelper = DataBaseHelper(this, DB_NAME)
        ItemDataset.mDbHelper = myDBHelper
        //Read data and print
        ItemDataset.TOPIC_ID = 1
        ItemDataset.MENU_ID = 1


        //Initialize the Admob Object and save it so that anybody can access it.
        AdObject.connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        AdObject.INTERSTITIAL_ID = AdObject.INTERSTITIAL_TEST_ID
        ItemDataset.APP_DIR = File(filesDir, "bookmarks")
        ItemDataset.APP_DIR.mkdirs()
        AdObject.PACKAGE_NAME = packageName

        /*--------START THE BACKGROUND SERVICE TO CHECK THE INTERNET CONNECTION---*/
//        startService(Intent(this,IntentServiceIsOnline::class.java))
//        startService(Intent(this,SimpleJobIntentService::class.java))
//        SimpleJobIntentService.enqueueWork(this,)

        AdObject.IsOnline.observeForever {
            if ((it == false) and (MSG_DISPLAYED == false)) {
                Snackbar.make(clMainActivity, "You are Offline.Please check internet connection.", Toast.LENGTH_SHORT).show()
                MSG_DISPLAYED = true
            } else if (it == true) MSG_DISPLAYED = false
        }
//        AdObject.IsOnline = IntentServiceIsOnline().isOnline()


        //Check Internet connection before loading the ad.
        loadBannerWithConnectivityCheck()
//        loadItemData()
//        Log.e("CLEAN-AFTER:",ItemDataset.items[0].menus.toString())

//      val jsondata = applicationContext.assets.open(ItemDataset.APP_JSON_FILE_NAME).bufferedReader().readText()

//        ItemDataset.items = streamingArray(jsondata)


        // If the app is in test mode load the test screen else load the splash screen.
        if (AdObject.showAppOrNot()) {
            if (AdObject.FRAGMENT_LOADED) {
                val prevScreen = AdObject.fragmentsStack.peek()
                when (prevScreen) {
                    START_SCREEN -> {
                        loadStartScreen()
                    }
                    TOPICS -> {
                        loadImageTopics()
                    }
                    MENUS -> loadMenus()
                    ITEM -> loadItem()
                    BOOKMARK_ITEM -> loadBookMarkItem()
                    BOOKMARK_MENU -> loadBookMarkMenu()
                }

            } else {
                loadSplashScreen()
            }
        } else {
            loadTestModeScreen()
        }

        Thread.setDefaultUncaughtExceptionHandler { t, e -> System.err.println(e.printStackTrace()) }

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        Log.d("Awww", "App in background")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        Log.d("Yeeey", "App in foreground")
    }

    /*----------------------ON BACK PRESS FOR THE ACTIVITY AND FRAGMENTS-------------------*/
    override fun onBackPressed() {
//        super.onBackPressed()
//        val i = Intent(Intent.ACTION_MAIN)
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

//        if (supportFragmentManager.backStackEntryCount > 0) {
//           // System.err.pritnln(supportFragmentManager.fragments.
//            supportFragmentManager.popBackStack();
//        } else {
//            super.onBackPressed();
//        }
        if (AdObject.fragmentsStack.size > 1) {
            // System.err.pritnln(supportFragmentManager.fragments.
            AdObject.fragmentsStack.pop()
            val prevScreen = AdObject.fragmentsStack.pop() as String

            when (prevScreen) {
                START_SCREEN -> {
                    loadStartScreen()
                }
                TOPICS -> {
                    loadImageTopics()
                }
                MENUS -> loadMenus()
                ITEM -> loadItem()
                BOOKMARK_ITEM -> loadBookMarkItem()
                BOOKMARK_MENU -> loadBookMarkMenu()
            }


        } else {
            AdObject.FRAGMENT_LOADED = false /*WHEN APP IS GOING INTO BACKGROUND, SET FRAGMENT_LOADED = FALSE*/
            AdObject.SPLASH_CALLED = false
            super.onBackPressed()
        }


//        finish()
    }

    /*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/
    /*-----------------------SCREEN 0 - THE SPLASH SCREEN----------------------*/
    override fun loadSplashScreen() {
        if (!isFinishing and !AdObject.SPLASH_CALLED) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, SplashFragment())
                commitAllowingStateLoss()

//                addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
            }


            this.loadBannerWithConnectivityCheck()
        }
    }

    /*-----------------------SCREEN 0 - THE TEST MODE SCREEN----------------------*/
    override fun loadTestModeScreen() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, TestModeFragment())
                commitAllowingStateLoss()
//                addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
            }

            this.loadBannerWithConnectivityCheck()
        }
    }

    /*-----------------------SCREEN 0 - THE MAIN SCREEN----------------------*/
    override fun loadStartScreen() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, StartScreenFragment())
                commitAllowingStateLoss()
//                    addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(START_SCREEN)
            loadBannerWithConnectivityCheck()
        }
    }

    override fun loadPrivacyPolicy() {
        startActivity(Intent(this, PrivacyPolicy::class.java))
    }

    /*-----------------------SCREEN 1 - THE IMAGE TOPICS----------------------*/
    override fun loadImageTopics() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, TopicFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(TOPICS)
            loadBannerWithConnectivityCheck()
        }
    }

    /*---------------------SCREEN 2 - IMAGE MENUS---------------------*/
    override fun loadMenus() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, MenuFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(MENUS)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded {loadMenus()}
        }
    }

    /*------------------------------SCREEN 3 - THE IMAGE ITEM------------------------*/
    override fun loadItem() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, ItemFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(ITEM)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadItem() }
        }
    }

    /*------------------------------SCREEN 4 - THE BOOK MARK MENU------------------------*/
    override fun loadBookMarkMenu() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, BookmarkFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_MENU)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadBookMarkMenu() }
        }
    }

    /*------------------------------SCREEN 5 - THE BOOK MARK ITEM------------------------*/
    override fun loadBookMarkItem() {
        if (!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, BookMarkItemFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_ITEM)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadBookMarkItem() }
        }
    }

/*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/


    fun parseJSON(array: String): Boolean {

        doAsync {
            val jsonArray = streamingArray(array)

        }
        runOnUiThread {
            alert("JSON parsing done!") {
                yesButton {
                    toast("Yat")
                    noButton { toast("haha") }
                }
            }
        }
        return true
    }


    fun streamingArray(array: String): Collection<Item> {


        val gson = Gson()

        // Deserialization
        val collectionType = object : TypeToken<Collection<Item>>() {}.type
        val result = gson.fromJson<Collection<Item>>(array, collectionType)


//
//        }
        return result
    }

    fun loadBannerWithConnectivityCheck() {
        if (AdObject.isConnected() and !adBanner.isLoading and !BANNER_LOADED) {


            val adRequest = AdRequest.Builder().build()
            adBanner.loadAd(adRequest)

            adBanner.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    // Toast.makeText(applicationContext, "Banner Loaded", Toast.LENGTH_SHORT).show()
                    Snackbar.make(clMainActivity, "Banner Loaded", Snackbar.LENGTH_SHORT).show()
                    BANNER_LOADED = true
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    // Code to be executed when an ad request fails.
                    // Toast.makeText(applicationContext, "Banner Failed:" + errorCode.toString(), Toast.LENGTH_SHORT).show()
                    Snackbar.make(clMainActivity, "Banner Failed:" + errorCode.toString(), Snackbar.LENGTH_SHORT).show()
                }

                override fun onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                }

                override fun onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                }

                override fun onAdClosed() {
                    // Code to be executed when when the user is about to return
                    // to the app after tapping on an ad.
                }
            }

        }
    }

    fun loadItemData() {
        ItemDataset.items = ArrayList<Item>()
        for (ele in 0.rangeTo(Images_Topics.topic_count - 1)) {
            ItemDataset.items.add(
                    Item(topic_title = Images_Topics.topic_titles[ele],
                            topic_icon = Images_Topics.topic_icons[ele],
                            menus = getImagesForItem(ele)
                    ))
        }

//        Log.e("DATA",ItemDataset.items.toString())
    }

    /*Input: INDEX for the Topic
    * Output: Array of the image names and URIS
    * */
    fun getImagesForItem(index_item: Int): ArrayList<String> {


        var menus: ArrayList<String> = ArrayList()
        val topic_size = ceil((images_all.size / Images_Topics.topic_count).toDouble()).toInt()

        val index_from = index_item * topic_size
        var index_to = index_from + topic_size - 1
        if (index_to > images_all.size - 1) {
            index_to = images_all.size - 1
        }
        menus.addAll(images_all.subList(index_from, index_to))
        return menus
    }

    /*--------------TO RESTORE THE SCREEN STATE ON RESUME---------------*/
    override fun onResume() {
        super.onResume()
        // If the app is in test mode load the test screen else load the start screen.
        if (AdObject.showAppOrNot()) {
            if (AdObject.FRAGMENT_LOADED == true) {
                val prevScreen = AdObject.fragmentsStack.peek()
                when (prevScreen) {
                    START_SCREEN -> {
                        loadStartScreen()
                    }
                    TOPICS -> {
                        loadImageTopics()
                    }
                    MENUS -> loadMenus()
                    ITEM -> loadItem()
                    BOOKMARK_ITEM -> loadBookMarkItem()
                    BOOKMARK_MENU -> loadBookMarkMenu()
                }

            } else {
                loadSplashScreen()
            }
        } else {
            loadTestModeScreen()
        }
    }
}

