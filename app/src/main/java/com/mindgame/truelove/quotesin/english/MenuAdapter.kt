package com.mindgame.truelove.quotesin.english

import android.content.Context
import android.net.Uri
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.layout_menu.view.*

class MenuAdapter(val ctx:Context, val act:FragmentActivity?, val appInterfaces: AppInterfaces):RecyclerView.Adapter<MenuAdapter.ViewHolder>() {


    class ViewHolder(val v: View) : RecyclerView.ViewHolder(v)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        val v = LayoutInflater.from(ctx).inflate(R.layout.layout_menu,parent,false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
         return ItemDataset.item_current.menus.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val imgPath = Uri.parse(ItemDataset.ASSET_URI + ItemDataset.item_current.menus[position])
        GlideApp.with(act as FragmentActivity)
                .load(imgPath)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.v.imgMenuIcon)

        holder.v.setOnClickListener(View.OnClickListener {

            ItemDataset.position = position


            if (act is AppInterfaces){
                // act.loadItem()
                AdObject.admob.loadNextScreen { appInterfaces.loadItem() }
            }
            

        })

    }
}