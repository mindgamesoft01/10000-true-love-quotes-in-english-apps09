package com.mindgame.truelove.quotesin.english

class NoDuplicateStack {
    val stack: MutableList<Any> = mutableListOf()
    val size: Int
        get() = stack.size

    // Push element onto the stack
    fun push(p: Any) {
        val index = stack.indexOf(p)
        if (index != -1) {
            stack.removeAt(index)
        }
        stack.add(p)
    }

    // Pop upper element of stack
    fun pop(): Any? {
        if (size > 0) {
            return stack.removeAt(stack.size - 1)
        } else {
            return null
        }
    }

    // Look at upper element of stack, don't pop it
    fun peek(): Any? {
        if (size > 0) {
            return stack[stack.size - 1]
        } else {
            return null
        }
    }
}

