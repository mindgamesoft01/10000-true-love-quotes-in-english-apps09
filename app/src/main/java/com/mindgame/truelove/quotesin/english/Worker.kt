package com.mindgame.truelove.quotesin.english

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.JobIntentService
import android.support.v4.os.ResultReceiver
import android.util.Log
import java.io.IOException
import java.util.*

/**
 * Created by Gokul on 2/25/2018.
 */

class Worker : JobIntentService() {
    /**
     * Result receiver object to send results
     */
    private var mResultReceiver: ResultReceiver? = null

    @SuppressLint("DefaultLocale", "RestrictedApi")
    override fun onHandleWork(intent: Intent) {
        Log.d(TAG, "onHandleWork() called with: intent = [$intent]")
        if (intent.action != null) {
            when (intent.action) {
                ACTION_DOWNLOAD -> {
                    mResultReceiver = intent.getParcelableExtra(RECEIVER)

                    val timerObj = Timer()
                    val timerTaskObj = object : TimerTask() {
                        override fun run() {
                            //perform your action here
                            AdObject.IsOnline.postValue(isOnline())
                        }
                    }
                    timerObj.schedule(timerTaskObj, 0, 10000)
                }
            }
        }
    }

    companion object {
        private val TAG = "Worker"
        val RECEIVER = "receiver"
        val SHOW_RESULT = 123
        /**
         * Unique job ID for this service.
         */
        internal val DOWNLOAD_JOB_ID = 1000
        /**
         * Actions download
         */
        private val ACTION_DOWNLOAD = "action.DOWNLOAD_DATA"

        /**
         * Convenience method for enqueuing work in to this service.
         */
        fun enqueueWork(context: Context, workerResultReceiver: WorkerResultReceiver) {
            val intent = Intent(context, Worker::class.java)
            intent.putExtra(RECEIVER, workerResultReceiver)
            intent.action = ACTION_DOWNLOAD
            JobIntentService.enqueueWork(context, Worker::class.java, DOWNLOAD_JOB_ID, intent)
        }
    }

    fun isOnline():Boolean {

        if (AdObject.connectivityManager.activeNetworkInfo == null || !AdObject.connectivityManager.activeNetworkInfo.isConnectedOrConnecting){
            return false
        }


        try {
            /*Pinging to Google server*/
            val command = "ping -c 1 google.com"
            val result = Runtime.getRuntime().exec(command).waitFor() == 0
            return result

        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e:InterruptedException) {
            e.printStackTrace()
        } finally {

        }
        return false
    }
    fun isConnected(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnectedOrConnecting
    }

}